import pandas as pd 

def leer_archivo(path, encode, separador):
    df= pd.read_csv(path, encoding= encode, sep=separador)
    return df 

def archivo_salida(dframe,path_output, encode, separador, index_tof, index_c):
    dframe.to_csv(path_output, encoding=encode, sep= separador, index=index_tof, index_label=index_c )
    print("se guardó con éxito como csv en "+ path_output)


#ingresar paths de los archivos
file_tsv='C:/Users/usuario/Desktop/datos_data_engineer.tsv'
output_csv='C:/Users/usuario/Desktop/datos_data_engineer.csv'

df=leer_archivo(file_tsv, 'utf_16_le', '\t')
archivo_salida(df, output_csv, 'utf-8','|',False, "id")


