CREATE TABLE listado_movimientos AS 
( 
SELECT 
m.fecha,
c.descripcion as descripcion_cliente,
pv.descripcion as descripcion_proveedor,
pd.descripcion as descripcion_producto,
ma.descripcion as descripcion_marca,
m.cantidad,
m.costo,
m.venta,
m.venta - m.costo as ganancia_neta
FROM Data_Movimientos m
JOIN Data_Clientes c ON m.cod_cliente=c.cod_cliente
JOIN Data_Productos pd ON m.cod_prod = pd.cod_prod
JOIN Data_Proveedores pv ON pd.cod_proveedor = pv.cod_proveedor
JOIN Data_Marcas ma ON pd.cod_marca= ma.cod_marca
);