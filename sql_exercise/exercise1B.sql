SELECT * 
FROM Data_Marcas ma
WHERE ma.cod_marca NOT IN (SELECT distinct mar.cod_marca
FROM Data_Movimientos m
JOIN Data_Productos pd ON m.cod_prod = pd.cod_prod 
JOIN Data_Marcas mar ON pd.cod_marca = mar.cod_marca);
