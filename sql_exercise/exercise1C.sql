SELECT * 
FROM (
SELECT lm.fecha, lm.descripcion_cliente, 
ROW_NUMBER() OVER(PARTITION BY lm.descripcion_cliente ORDER BY lm.fecha DESC) AS row_number, 
SUM(lm.ganancia_neta) OVER (PARTITION BY lm.descripcion_cliente ORDER BY lm.fecha DESC) AS ganancia_neta_acumulada
FROM listado_movimientos lm
) 
WHERE row_number= 1
LIMIT 7;